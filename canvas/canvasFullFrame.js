const $canvas = document.createElement('canvas');
const ctx = $canvas.getContext('2d', { alpha: false });

const width = 256;
const height = 240;

$canvas.width = width;
$canvas.height = height;

document.body.append($canvas);


let lastTime = performance.now();

const loop = () => {
  const framerate = Math.floor(1000/(performance.now() - lastTime));

  ctx.fillStyle = 'black';
  ctx.fillRect(0, 0, width, height);

  for (let x = 0; x < width; x += 1) {
    for (let y = 0; y < height; y += 1) {
      ctx.fillStyle = Math.random() > .5 ? 'black' : 'white';
      ctx.fillRect(x, y, 1, 1);
    }
  }

  ctx.font = "30px Arial";
  ctx.textAlign = "center";
  ctx.fillStyle = 'red';
  ctx.fillText(framerate, width / 2, height / 2);

  lastTime = performance.now();
  window.requestAnimationFrame(loop);
}

loop();
