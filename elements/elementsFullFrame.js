const width = 256;
const height = 240;

const $frame = document.createElement('div');
$frame.style.display = 'grid';
$frame.style.gridTemplateColumns = `repeat(${256}, 1px)`;
document.body.append($frame);

const $frameRateDiv = document.createElement('div');
const $frameRateCanvas = document.createElement('canvas');
const ctx = $frameRateCanvas.getContext('2d', { alpha: false });
document.body.append($frameRateDiv);
document.body.append($frameRateCanvas);

const frameBuffer = new Array();
const elementsBuffer = new Array();

let lastTime = performance.now();

const initializeArray = () => {
  for (let x = 0; x < width; x += 1) {
    frameBuffer[x] = new Array();
  }
}

const initializeFrame = () => {
  const t = performance.now();
  for (let i = 0; i < width * height; i += 1) {
    const $tmp = document.createElement('div');
    $tmp.style.width = '1px';
    $tmp.style.height = '1px';
    $tmp.style.backgroundColor = 'black';

    const x = Math.floor(i % width);
    const y = Math.floor(i / width);

    $tmp.className = `x-${x} y-${y} i-${i}`;

    $frame.append($tmp);

    elementsBuffer[(x * width) + y] = $tmp;
  }
  console.log(`Load time: ${Math.floor(performance.now() - t)}ms`);
}

const loop = () => {
  const framerate = Math.floor(1000/(performance.now() - lastTime));

  for (let x = 0; x < width; x += 1) {
    for (let y = 0; y < height; y += 1) {
      const pixel = Math.random() < .5 ? 'black' : 'white';
      elementsBuffer[(x * width) + y].style.backgroundColor = pixel;
    }
  }

  // This was SO slow (6fps)
  // $frameRateDiv.innerText = framerate;

  ctx.clearRect(0, 0, 100, 100);
  ctx.font = "30px Arial";
  ctx.textAlign = "center";
  ctx.fillStyle = 'red';
  ctx.fillText(framerate, 20, 20);

  lastTime = performance.now();
  window.requestAnimationFrame(loop);
}


initializeArray();
initializeFrame();

loop();
